/**
 * 
 */
package com.yevelop.webserviceConsumer.client;

import java.time.LocalDateTime;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;


import com.yevelop.webserviceConsumer.dto.EmpleadoDTO;

/**
 * @author Jose Antonio
 * 
 * Clase Cliente para Consumir webserice de Empleados
 *
 */
public class EmpleadoWSClient {
	
	
	public static void main(String[] args) {
//		Client client = ClientBuilder.newClient();
//		WebTarget webTarget = client.target("http://localhost:8080/project-webservices/yepes/empleadosWS").path("consultarEmpleadosPorID").path("123");
//		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
//		Response response = invocationBuilder.get();
//		
//		if(response.getStatus() == 204) {
//			System.out.println("No se encontro el empleado");
//		}
//		
//		if(response.getStatus()==200) {
//			EmpleadoDTO empleado = response.readEntity(EmpleadoDTO.class);
//			System.out.println(" Web service del Empleado " + empleado.getNombre());
//			System.out.println(" Fecha " + empleado.getFechaCreacion());
//			
//		}
		
//		EmpleadoDTO empleado = response.readEntity(EmpleadoDTO.class);
//		System.out.println(" Web service del Empleado " + empleado.getNombre());
		
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://localhost:8080/project-webservices/yepes/empleadosWS").path("guardarEmpleado");
		
		EmpleadoDTO empleado = new EmpleadoDTO();
		empleado.setNumeroEmpleado("1");
		empleado.setNombre("Laura");
		empleado.setPrimerApellido("Croft");
		empleado.setSegundoApellido("Rider");
		empleado.setFechaCreacion(LocalDateTime.now());
		empleado.setEdad(22);
		empleado.setFechaCreacion(LocalDateTime.now());
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(empleado, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() == 400) {
			String error = response.readEntity(String.class);
			System.out.println(error);
		}
		
		if(response.getStatus() == 200) {
			EmpleadoDTO emp = response.readEntity(EmpleadoDTO.class);
			System.out.println(emp.getNombre());
			System.out.println(emp.getFechaCreacion());
		}
	
	}
	
}
